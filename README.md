# This is the repository for the branding images for Cloverleaf Linux.  The images currently existing are the default openSUSE images, and are there for reference only.


#Cloverleaf Linux Artwork Guideline

#It’s true that Linux appearance needs tweaks to provide a good look & feel.

#But it’s false that everyone’s tweaks can be used to make a distribution.

#A distribution’s artwork guideline is to find a balance so that everyone can contribute and to reduce the number of unusable submissions. So we need a standard to filter those reproducible ideas.

#So here’s our guideline:

#1. The main color: white, black, green, and those on our logo.

#We all know what “main” means. You can’t make an orange wallpaper and say: here’s my submit.

#2. Your submit must be “related”.

#It’s not required to have our logo on your artwork, but an artwork without a proper logo means it’s supplemental instead of default. 

#It’s required to have something related with “cloverleaf” or even “leaf”. we’re not a “general” Linux anyway.

#If we have a meaningful codename for a specific release, artworks can be designed around the meaning of the codename. But in the next release with a difference codename, such artwork is not reusable.

#3. Your submit must be “libre” and “open source”.

#You can’t just find a good-looking photograph over the internet, PS our logo on it, and submit.

#No matter how amazing it is, we can’t use it. Because we don’t have a redistribution license with the original author. 

#If you’re the author (welcome, photographers!), you must release your artwork under CC-BY-SA or other open source compatible license like GPL/Public Domain. If you’re not, talk to the author and teach him libre and open source culture.

#4. You submit must be “scalable” and you must provide the source.

#Personally I do want to enforce everyone submit “.svg” artworks and design with inkscape. But it’s unreal. Some of you may only know how to photoshop.

#But keep this in mind: We’re a distribution. We don’t know what screen resolution it will be on end user’s laptop.

#So you must provide artworks for every screen resolution. (4:3 and 16:9). The bigger, the better, because big one resizes good while small one don’t. And it’s your job to find how many resolutions there are for a computer. 

#http://en.wikipedia.org/wiki/Display_resolution

#Well, prepare for retina display! Apple released 5120x2880 this year.

#If it’s really a pain to resize for so many resolutions, I suggest it’s time to have a look at inkscape. Using it, you only need to provide two ratios: 16:9 and 4:3, then you can produce any size at that ratio.

#5. Your submit must be as “professional” as you can.

#Everyone has his limitation. That’s true.


#What we need:

#Main: Wallpaper, KDE colorscheme, KDE window style, KDE workspace skin, SDDM design, KDE Splash, grub2 splash, plymouth splash. 

#Minor: Libreoffice splash/GIMP splash (If we have them in our default software selection), Icons.

#Not everyone can actually _make_ such things, although there’re tutorials everywhere on the internet.

#But you can suggest good ones and even tweak a little bit based on them.

#What we still need:

#Posters/Stickers/G+ background images/Wordpress skin. anything that can brand us is welcome.

#Where to submit:

#We have a group #cloverleaf-linux on deviantart.com http://cloverleaf-linux.deviantart.com/ for ideas, and we have a git repo for final work https://bitbucket.org/sfalken/cloverleaf-images/overview.  Additionally, if you aren’t into using git, you can e-mail your submissions to team@cloverleaf-linux.org

# You can submit your artworks there.
#If you’re just recommending something, post it on G+ cloverleaf-linux community.

